package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.PlatformmanagementManager;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvidePlatformmanagementManagerFactory
    implements Factory<PlatformmanagementManager> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvidePlatformmanagementManagerFactory(
      PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public PlatformmanagementManager get() {
    return provideInstance(module);
  }

  public static PlatformmanagementManager provideInstance(PlatformmanagementModule module) {
    return proxyProvidePlatformmanagementManager(module);
  }

  public static PlatformmanagementModule_ProvidePlatformmanagementManagerFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvidePlatformmanagementManagerFactory(module);
  }

  public static PlatformmanagementManager proxyProvidePlatformmanagementManager(
      PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.providePlatformmanagementManager(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
