package com.reds.platform.platformmanagement.inject;

import com.reds.platform.redsplatformcommons.command.executor.ServiceCommandExecutor;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvideServiceCommandExecutorFactory
    implements Factory<ServiceCommandExecutor> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvideServiceCommandExecutorFactory(
      PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public ServiceCommandExecutor get() {
    return provideInstance(module);
  }

  public static ServiceCommandExecutor provideInstance(PlatformmanagementModule module) {
    return proxyProvideServiceCommandExecutor(module);
  }

  public static PlatformmanagementModule_ProvideServiceCommandExecutorFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvideServiceCommandExecutorFactory(module);
  }

  public static ServiceCommandExecutor proxyProvideServiceCommandExecutor(
      PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.provideServiceCommandExecutor(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
