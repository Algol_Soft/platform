package com.reds.platform.platformmanagement.command.cmd;

import dagger.internal.Factory;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformCommand_Factory implements Factory<PlatformCommand> {
  private static final PlatformCommand_Factory INSTANCE = new PlatformCommand_Factory();

  @Override
  public PlatformCommand get() {
    return provideInstance();
  }

  public static PlatformCommand provideInstance() {
    return new PlatformCommand();
  }

  public static PlatformCommand_Factory create() {
    return INSTANCE;
  }

  public static PlatformCommand newPlatformCommand() {
    return new PlatformCommand();
  }
}
