package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.command.CommandShell;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvideCommandShellFactory
    implements Factory<CommandShell> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvideCommandShellFactory(PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public CommandShell get() {
    return provideInstance(module);
  }

  public static CommandShell provideInstance(PlatformmanagementModule module) {
    return proxyProvideCommandShell(module);
  }

  public static PlatformmanagementModule_ProvideCommandShellFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvideCommandShellFactory(module);
  }

  public static CommandShell proxyProvideCommandShell(PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.provideCommandShell(), "Cannot return null from a non-@Nullable @Provides method");
  }
}
