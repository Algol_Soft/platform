package com.reds.platform.platformmanagement;

import com.reds.platform.platformmanagement.inject.PlatformmanagementTreasury;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;

public class Platform {
	
	

	public static void main(String[] args) {		
		try {
			/**Temp Hack for Supress Warning*/
			disableWarning();
			//System.out.println("Shineed");
			PlatformmanagementTreasury.init();
			PlatformmanagementManager manager = PlatformmanagementTreasury.open.takePlatformmanagementManager();			
			PlatformmanagementTrack.me.info("_______________Starting Platform_______________");
			
			
			
			manager.start();
		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to start platform management", e);
		}
	}

	public static void disableWarning() {
	    System.err.close();
	    System.setErr(System.out);
	}
}
