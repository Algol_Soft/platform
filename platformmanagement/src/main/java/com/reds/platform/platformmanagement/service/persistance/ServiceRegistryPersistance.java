package com.reds.platform.platformmanagement.service.persistance;

import java.util.List;

import com.reds.platform.platformmanagement.exception.ServiceManagementException;

public interface ServiceRegistryPersistance {

	public void save(ServiceInfoPersistanceObject object) throws ServiceManagementException;

	public void delete(ServiceInfoPersistanceObject object) throws ServiceManagementException;

	public List<ServiceInfoPersistanceObject> getAll() throws ServiceManagementException;

	

}
