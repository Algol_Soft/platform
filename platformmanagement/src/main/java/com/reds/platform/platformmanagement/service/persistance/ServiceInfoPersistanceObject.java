package com.reds.platform.platformmanagement.service.persistance;

import com.reds.library.redscommons.config.PlatformConfig;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;

public class ServiceInfoPersistanceObject implements PlatformConfig {

	private ServiceInformation information;

	@Override
	public String getId() {

		return information.getMetaData().getName() + "_" + information.getMetaData().getVersion();
	}

	public final ServiceInformation getInformation() {
		return information;
	}

	public final void setInformation(ServiceInformation information) {
		this.information = information;
	}

	@Override
	public String getDescription() {
		return "deployment details " + this.information.getMetaData().getName() + "_"
				+ this.information.getMetaData().getVersion();
	}

}
