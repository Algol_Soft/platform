package com.reds.platform.platformmanagement.service;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarFile;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.io.FileUtils;
import org.joor.Reflect;

import com.reds.library.redsutils.RedsDateUtils;
import com.reds.platform.platformmanagement.PlatformManagementUtils;
import com.reds.platform.platformmanagement.exception.PlatformmanagementErrorCodes;
import com.reds.platform.platformmanagement.exception.ServiceManagementException;
import com.reds.platform.platformmanagement.inject.PlatformmanagementTreasury;
import com.reds.platform.platformmanagement.service.persistance.ServiceInfoPersistanceObject;
import com.reds.platform.platformmanagement.service.persistance.ServiceRegistryPersistance;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;
import com.reds.platform.redsplatformcommons.service.ServiceMetaData;
import com.reds.platform.redsplatformcommons.service.ServiceStatus;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.platform.servicecommons.Service;
import com.reds.platform.servicecommons.ServiceLookUp;
import com.reds.platform.servicecommons.context.DeploymentContext;
import com.reds.platform.servicecommons.context.ServiceContext;
import com.reds.platform.servicecommons.exception.ServiceException;
import com.reds.platform.servicecommons.resilience.ResilienceCallBack;
import com.reds.platform.servicecommons.resilience.ResilienceObect;

@Singleton
public class ServiceRegistryImpl implements ServiceRegistry, ServiceLookUp {

	final private Map<String, ServiceRegistryEntry> registry;

	private URLClassLoader serviceClassLoader = null;

	@Inject
	public ServiceRegistryImpl() {
		this.registry = new ConcurrentHashMap<>();
		this.serviceClassLoader = createClasLoader();

	}

	private URLClassLoader createClasLoader() {
		URLClassLoader serviceClassLoader = URLClassLoader.newInstance(new URL[0], getClass().getClassLoader());
		Thread.currentThread().setContextClassLoader(serviceClassLoader);
		return serviceClassLoader;

	}

	@Override
	public void register(ServiceInformation information, boolean classLoadRequired) throws ServiceException {
		ServiceMetaData metaData = information.getMetaData();
		ServiceType type = metaData.getType();
		String serviceKey = getServiceKey(metaData.getName(), metaData.getVersion());

		String serviceLibs = information.getServiceLibs();
		String serviceConfigLocation = information.getServiceConfigLocation();

		if (type == ServiceType.BUSINESS) {

		} else {

		}
		ServiceRegistryEntry registryEntry = null;
		if (classLoadRequired) {
			try {
				URLClassLoader serviceClassLoader = getServiceClassLoader(metaData, serviceLibs,
						information.getServiceLocation());

				@SuppressWarnings("unchecked")
				// Class<Service> clazz = (Class<Service>)
				// Class.forName(metaData.getImplementation(), true,
				// serviceClassLoader);

				Class<Service> clazz = Reflect.onClass(metaData.getImplementation(), serviceClassLoader).get();

				// Service service = clazz.newInstance();
				Service service = (Service) clazz.getDeclaredConstructors()[0].newInstance();
				registryEntry = new ServiceRegistryEntry(serviceKey, service, serviceClassLoader, information);
				DeploymentContext deploymentInfo = new DeploymentContext(type, metaData.getName(),
						metaData.getVersion(), information.getServiceLocation(), serviceConfigLocation, serviceLibs);
				service.deployed(deploymentInfo);
			} catch (Exception e) {
				PlatformmanagementTrack.me.error("Failed to create instance of Service {}", serviceKey, e);
			}

		} else {

			registryEntry = new ServiceRegistryEntry(serviceKey, null, null, information);

		}

		this.registry.put(serviceKey, registryEntry);

	}

	private URLClassLoader getServiceClassLoader(ServiceMetaData metaData, String serviceLibs, String serviceRoot) {
		Collection<File> paths;
		String extensions[] = { "jar" };
		paths = FileUtils.listFiles(new File(serviceRoot), extensions, true);
		Method method = null;

		try {

			method = serviceClassLoader.getClass().getSuperclass().getDeclaredMethod("addURL",
					new Class[] { URL.class });
			method.setAccessible(true);

			for (File path : paths) {
				// PlatformmanagementTrack.me.debug("Adding {} to platform ", path);
				method.invoke(serviceClassLoader, path.toURI().toURL());
			}
		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to add dependent libraries to class path", e);
		}
		return serviceClassLoader;
	}

	private String getServiceKey(String name, String version) {
		return PlatformManagementUtils.getServiceKey(name, version);
	}

	@Override
	public ServiceInformation unRegister(String name, String version) throws ServiceException {
		ServiceInformation information = this.registry.get(getServiceKey(name, version)).getServiceInfo();

		ServiceRegistryEntry entry = getServiceEntry(name, version);
		try {
			entry.getServiceClassLoader().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.registry.remove(getServiceKey(name, version));
		return information;

	}

	@Override
	public ServiceRegistryEntry getServiceEntry(String name, String version) {
		ServiceRegistryEntry entry = this.registry.get(getServiceKey(name, version));
		return entry;
	}

	@Override
	public Service getService(String name, String version) throws ServiceException {
		ServiceRegistryEntry entry = getServiceEntry(name, version);
		if (entry == null) {
			return null;
		}

		return entry.getService();
	}

	@Override
	public List<ServiceInformation> getAllServiceInfo() throws ServiceException {
		List<ServiceInformation> informations = new ArrayList<>();
		for (ServiceRegistryEntry entry : this.registry.values()) {
			informations.add(entry.getServiceInfo());
		}
		return informations;
	}

	@Override
	public void setStatus(ServiceStatus status, String name, String version) throws ServiceManagementException {
		ServiceRegistryEntry entry = this.registry.get(getServiceKey(name, version));
		if (entry != null) {
			entry.getServiceInfo().setLastUpdatedTime(RedsDateUtils.getNow().getTime());
			entry.getServiceInfo().setStatus(status);
			persistServiceInformation(entry.getServiceInfo());
		}
	}

	private void persistServiceInformation(ServiceInformation information) throws ServiceManagementException {
		ServiceRegistryPersistance registryPersistance = PlatformmanagementTreasury.open
				.takeServiceRegistryPersistance();
		ServiceInfoPersistanceObject persistenceObject = new ServiceInfoPersistanceObject();
		persistenceObject.setInformation(information);
		registryPersistance.save(persistenceObject);
	}

	@Override
	public void load() throws ServiceException {
		try {
			ServiceRegistryPersistance registryPersistance = PlatformmanagementTreasury.open
					.takeServiceRegistryPersistance();
			List<ServiceInfoPersistanceObject> persistenceObjects = registryPersistance.getAll();
			for (ServiceInfoPersistanceObject persistanceObject : persistenceObjects) {
				if (persistanceObject.getInformation().getStatus() != ServiceStatus.UNDEPLOYED) {
					register(persistanceObject.getInformation(), true);
				}
			}
		} catch (ServiceManagementException e) {
			throw new ServiceException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to load persistence service objects ", e);
		}

	}

	@Override
	public void stop() throws ServiceManagementException {
		if (registry != null) {
			for (ServiceRegistryEntry entry : registry.values()) {
				try {
					PlatformmanagementTrack.me.info("Stopping {}", entry.getServiceKey());
					/* TODO need to add required elements to RedsContext */
					ServiceContext context = new ServiceContext((ServiceLookUp) this,
							entry.getServiceInfo().getServiceConfigLocation());
					context.setServiceName(entry.getServiceInfo().getMetaData().getName());
					context.setServiceVersion(entry.getServiceInfo().getMetaData().getVersion());
					entry.getService().stop(context);
				} catch (Exception e) {
					PlatformmanagementTrack.me.error("Stopping failed {} ",entry.getServiceKey(), e);
				}
			}
		}

	}

	@Override
	public Service getService(String name, String version, ResilienceCallBack callBack) {
		Service service = null;
		try {
			service = getService(name, version);
			if (service == null) {
				ResilienceObect resilienceObect = new ResilienceObect();
				resilienceObect.setName(name);
				resilienceObect.setVersion(version);
				callBack.onServiceNotFound(resilienceObect);
			}
			return service;
		} catch (Exception e) {
			ResilienceObect resilienceObect = new ResilienceObect();
			resilienceObect.setName(name);
			resilienceObect.setVersion(version);
			callBack.onServiceInvocationException(resilienceObect, e);
		}
		return service;
	}

	@Override
	public void printLoadedClass() throws ServiceManagementException {
		// for (Class clazz : this.serviceClassLoader.getLoadedClasses().values()) {
		// System.out.println(">>> " + clazz.getName());
		// }

	}

}
