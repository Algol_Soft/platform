package com.reds.platform.platformmanagement.service;

import java.net.URLClassLoader;

import org.xeustechnologies.jcl.JarClassLoader;

import com.reds.library.redscommons.to.PlatformTO;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;
import com.reds.platform.redsplatformcommons.service.ServiceStatus;
import com.reds.platform.servicecommons.Service;

public class ServiceRegistryEntry implements PlatformTO {

	final private String serviceKey;

	final private Service service;

	final private URLClassLoader serviceClassLoader;

	final private ServiceInformation serviceInfo;

	/**
	 * @param serviceKey
	 * @param service
	 */
	public ServiceRegistryEntry(String serviceKey, Service service, URLClassLoader serviceClassLoader,
			ServiceInformation serviceInfo) {
		this.serviceKey = serviceKey;
		this.service = service;
		this.serviceClassLoader = serviceClassLoader;
		this.serviceInfo = serviceInfo;
	}

	@Override
	final public String getId() {
		return serviceKey;
	}

	final public String getServiceKey() {
		return serviceKey;
	}

	final public Service getService() {
		return service;
	}

	final public URLClassLoader getServiceClassLoader() {
		return serviceClassLoader;
	}

	public final ServiceInformation getServiceInfo() {
		return serviceInfo;
	}

}
