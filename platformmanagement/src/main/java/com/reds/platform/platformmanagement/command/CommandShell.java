package com.reds.platform.platformmanagement.command;

import java.nio.file.Files;
import java.nio.file.Paths;

import javax.inject.Singleton;

import com.budhash.cliche.ShellFactory;
import com.reds.library.redsutils.OsUtils;
import com.reds.platform.platformmanagement.inject.PlatformmanagementTreasury;

@Singleton
public class CommandShell {

	private String welcomeMessage =readBanner("./config/banner.txt");//RedsFileUtils.readBanner(PlatformCommand.class, "banner.txt");

	private String prompt = "platform";

	public void start() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					ShellFactory.createConsoleShell(prompt, welcomeMessage,
							PlatformmanagementTreasury.open.takePlatformCommand()).commandLoop();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}).start();

	}
	
	private static String readBanner(String path) {
		try {
			String content = readFileAsString(path);
			return content.replaceAll("(\\r|\\n)+", OsUtils.LINE_SEPARATOR);
		} catch (Exception ex) {
			throw new IllegalStateException("Cannot read stream", ex);
		}
	}
	
	public static String readFileAsString(String fileName)throws Exception 
	  { 
	    String data = ""; 
	    data = new String(Files.readAllBytes(Paths.get(fileName))); 
	    return data; 
	  } 

}
