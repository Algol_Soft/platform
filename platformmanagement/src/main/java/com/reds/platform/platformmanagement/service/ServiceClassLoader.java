package com.reds.platform.platformmanagement.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

public class ServiceClassLoader extends URLClassLoader {
	
	private static final Class[] parameters = new Class[]{URL.class};

	public ServiceClassLoader(URL[] urls, ClassLoader parent) {
		super(urls, parent);
	}

	@Override
	public void addURL(URL url) {	
		super.addURL(url);

		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
		try {
		    Method method = classLoader.getClass().getDeclaredMethod("addURL", URL.class);
		    method.setAccessible(true);
		    method.invoke(classLoader, url);
		} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			try {
		    Method method = classLoader.getClass()
		            .getDeclaredMethod("appendToClassPathForInstrumentation", String.class);
		    method.setAccessible(true);
		    method.invoke(classLoader, url.getPath());
			}catch(Throwable t) {
				t.printStackTrace();
			}
			
		}
//		  ClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
//		  Class sysclass = URLClassLoader.class;
//		try {
//		     Method method = sysclass.getDeclaredMethod("addURL", parameters);
//		     System.out.println(url.getPath());
//		     method.setAccessible(true);
//		     method.invoke(sysloader, new Object[]{url});
//		  } catch (Throwable t) {
//		     t.printStackTrace();
//		  }
	}

}