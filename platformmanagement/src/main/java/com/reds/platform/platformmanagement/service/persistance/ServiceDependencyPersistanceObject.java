package com.reds.platform.platformmanagement.service.persistance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.reds.library.redscommons.config.PlatformConfig;

public class ServiceDependencyPersistanceObject implements PlatformConfig {

	private Map<String, String> libraryToServices;

	private String id = "ServiceDependencies";

	public ServiceDependencyPersistanceObject() {
		this.libraryToServices = new HashMap<>();
	}

	public void addDependency(String libaryName, String serviceKey) {
		String listOfService = this.libraryToServices.get(libaryName);
		if (null == listOfService) {
			listOfService = "";
		}
		String[] services = listOfService.split(",");
		boolean alreadyExist = false;
		for (String service : services) {
			if (service.equals(serviceKey)) {
				alreadyExist = true;
			}
		}
		if (!alreadyExist) {
			listOfService = listOfService + serviceKey + ",";
		}

		this.libraryToServices.put(libaryName, listOfService);

	}

	/**
	 * <b>Purpose:</b> To get list of libraries to be removed from platform lib
	 * folder
	 * 
	 * @param serviceKey - ServiceName and Service Version of the Service which is
	 *                   unDeploying
	 * @return List of Libraries Name, Which is supposed to be removed from platform
	 *         lib folder
	 */
	/**
	 * @param serviceKey
	 * @return
	 */
	public List<String> removeDependencyOfService(String serviceKey) {
		List<String> librariesToRemove = new ArrayList<>();
		/* Collecting List of Libraries to be removed */
		for (Entry<String, String> entrySet : this.libraryToServices.entrySet()) {
			String[] services = entrySet.getValue().split(",");
			StringBuilder newServices = new StringBuilder();
			for (String service : services) {
				if (!service.equals(serviceKey)) {
					newServices.append(service).append(",");
				}
			}
			String libraryName = entrySet.getKey();
			String newServiceString = newServices.toString();
			/* Expecting a empty comma Alone. Then also we can ignore this lib */
			if (newServiceString.length() >=0) {
				
				librariesToRemove.add(libraryName);
			} else {
				this.libraryToServices.put(libraryName, newServiceString);
			}

		}
		for(String removeLib:librariesToRemove) {
			this.libraryToServices.remove(removeLib);
		}

		/* Return List of Libraries to be removed from lib folder */
		return librariesToRemove;

	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getDescription() {

		return "Holds all System Specific Service Dependencies, which required platform to manage";
	}

	public Map<String, String> getLibraryToServices() {
		return libraryToServices;
	}

	public void setLibraryToServices(Map<String, String> libraryToServices) {
		this.libraryToServices = libraryToServices;
	}

	@Override
	public PlatformConfig getDefault() {

		return this;
	}
}
