package com.reds.platform.platformmanagement;

import com.reds.platform.platformmanagement.exception.PlatformmanagementException;
import com.reds.platform.platformmanagement.inject.PlatformmanagementTreasury;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;

public class PlatformShutDownHook extends Thread {

	public void run() {
		PlatformmanagementTrack.me.info("Platform Stop Initiated......");
		PlatformmanagementManager manager = PlatformmanagementTreasury.open.takePlatformmanagementManager();
		try {
			if (manager.stop()) {
				PlatformmanagementTrack.me.info("______________ Platform Stopped______________");
			} else {
				PlatformmanagementTrack.me.warn("Abnormal Stop detected");
			}
		} catch (PlatformmanagementException e) {
			PlatformmanagementTrack.me.error("Failed to stop platform gracefully ", e);
		}

	}
}
