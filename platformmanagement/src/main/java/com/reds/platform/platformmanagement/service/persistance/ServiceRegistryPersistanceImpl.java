package com.reds.platform.platformmanagement.service.persistance;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.reds.library.redscommons.config.PlatformConfig;
import com.reds.library.redscommons.exception.RedscommonsException;
import com.reds.platform.platformmanagement.exception.PlatformmanagementErrorCodes;
import com.reds.platform.platformmanagement.exception.ServiceManagementException;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;
import com.reds.platform.redsplatformcommons.config.AbstractConfigurationManager;
import com.reds.platform.redsplatformcommons.inject.RedsplatformcommonsTreasury;

@Singleton
public class ServiceRegistryPersistanceImpl extends AbstractConfigurationManager implements ServiceRegistryPersistance {

	@Inject
	public ServiceRegistryPersistanceImpl() {
		super(RedsplatformcommonsTreasury.open.takePlatformLayout().getServiceRegistryPersistance());
		super.registerConfig(ServiceInfoPersistanceObject.class);
		
	}

	@Override
	public void save(ServiceInfoPersistanceObject object) throws ServiceManagementException {
		try {
			super.write(object);
		} catch (RedscommonsException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to save persistence metadata", e);
		}
	}

	@Override
	public void delete(ServiceInfoPersistanceObject object) throws ServiceManagementException {
		try {
			super.delete(object);
		} catch (RedscommonsException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to save persistence metadata", e);
		}

	}

	@Override
	public List<ServiceInfoPersistanceObject> getAll() throws ServiceManagementException {
		try {
			List<ServiceInfoPersistanceObject> objects = new ArrayList<>();
			List<PlatformConfig> configs = super.readAll(ServiceInfoPersistanceObject.class);
			for (PlatformConfig redsConfig : configs) {
				objects.add((ServiceInfoPersistanceObject) redsConfig);
			}
			return objects;

		} catch (RedscommonsException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to read all persistence metadata", e);
		}

	}

	
	

}
