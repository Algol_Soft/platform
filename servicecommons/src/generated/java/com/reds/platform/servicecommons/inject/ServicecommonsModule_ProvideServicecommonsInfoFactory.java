package com.reds.platform.servicecommons.inject;

import com.reds.platform.servicecommons.info.ServicecommonsInfo;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ServicecommonsModule_ProvideServicecommonsInfoFactory
    implements Factory<ServicecommonsInfo> {
  private final ServicecommonsModule module;

  public ServicecommonsModule_ProvideServicecommonsInfoFactory(ServicecommonsModule module) {
    this.module = module;
  }

  @Override
  public ServicecommonsInfo get() {
    return provideInstance(module);
  }

  public static ServicecommonsInfo provideInstance(ServicecommonsModule module) {
    return proxyProvideServicecommonsInfo(module);
  }

  public static ServicecommonsModule_ProvideServicecommonsInfoFactory create(
      ServicecommonsModule module) {
    return new ServicecommonsModule_ProvideServicecommonsInfoFactory(module);
  }

  public static ServicecommonsInfo proxyProvideServicecommonsInfo(ServicecommonsModule instance) {
    return Preconditions.checkNotNull(
        instance.provideServicecommonsInfo(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
