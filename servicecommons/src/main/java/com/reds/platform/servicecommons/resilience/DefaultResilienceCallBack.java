package com.reds.platform.servicecommons.resilience;

import com.reds.platform.servicecommons.tracker.ServicecommonsTrack;

public class DefaultResilienceCallBack implements ResilienceCallBack {

	@Override
	public void onServiceNotFound(ResilienceObect resilienceObect) {
		ServicecommonsTrack.me.error("Service {}:{} Not found ",resilienceObect.getName(),resilienceObect.getVersion());
	}

	@Override
	public void onServiceInvocationException(ResilienceObect resilienceObect, Exception exception) {
		ServicecommonsTrack.me.error("Exception while invocation of service {}:{} Not found ",resilienceObect.getName(),resilienceObect.getVersion(),exception);
		
	}

}
