package com.reds.platform.servicecommons;

import com.reds.platform.servicecommons.context.ServiceContext;

public abstract class AbstractServiceProvider {
	
	private ServiceContext serviceContext;

	public AbstractServiceProvider(ServiceContext serviceContext) {
		super();
		this.serviceContext = serviceContext;
	}

	public ServiceContext getServiceContext() {
		return serviceContext;
	}
	

}
