package com.reds.platform.servicecommons;

import com.reds.platform.redsplatformcommons.config.AbstractConfigurationManager;
import com.reds.platform.servicecommons.context.ServiceContext;

public abstract class AbstractServiceManager {

	private AbstractConfigurationManager configurationManager;

	public AbstractServiceManager(ServiceContext context) {
		this.configurationManager = new ServiceConfigManager(context.getConfigLocation());
	}

	public void registerConfig(Class clazz) {
		configurationManager.registerConfig(clazz);
	}

	public AbstractConfigurationManager getConfigurationManager() {
		return configurationManager;
	}

	public void setConfigurationManager(AbstractConfigurationManager configurationManager) {
		this.configurationManager = configurationManager;
	}

}
