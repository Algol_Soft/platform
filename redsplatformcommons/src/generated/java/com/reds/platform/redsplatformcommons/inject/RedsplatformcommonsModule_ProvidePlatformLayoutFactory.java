package com.reds.platform.redsplatformcommons.inject;

import com.reds.platform.redsplatformcommons.layout.PlatformLayout;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RedsplatformcommonsModule_ProvidePlatformLayoutFactory
    implements Factory<PlatformLayout> {
  private final RedsplatformcommonsModule module;

  public RedsplatformcommonsModule_ProvidePlatformLayoutFactory(RedsplatformcommonsModule module) {
    this.module = module;
  }

  @Override
  public PlatformLayout get() {
    return provideInstance(module);
  }

  public static PlatformLayout provideInstance(RedsplatformcommonsModule module) {
    return proxyProvidePlatformLayout(module);
  }

  public static RedsplatformcommonsModule_ProvidePlatformLayoutFactory create(
      RedsplatformcommonsModule module) {
    return new RedsplatformcommonsModule_ProvidePlatformLayoutFactory(module);
  }

  public static PlatformLayout proxyProvidePlatformLayout(RedsplatformcommonsModule instance) {
    return Preconditions.checkNotNull(
        instance.providePlatformLayout(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
