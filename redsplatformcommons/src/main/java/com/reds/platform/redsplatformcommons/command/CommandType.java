package com.reds.platform.redsplatformcommons.command;

public enum CommandType {
	INFO("info"),LOGIN("login"), LOGOUT("logout"), DEPLOY("deploy"), UN_DEPLOY("undeploy"), START_SERVICE("start"),
	STOP_SERVICE("start"), STATUS("status"), LIST("list"),LOG_PATH("logs"),SYSTEM_INFO("systeminfo");

	private String command;

	private CommandType(String command) {
		this.command = command;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
