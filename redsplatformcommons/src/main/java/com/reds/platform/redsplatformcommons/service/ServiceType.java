package com.reds.platform.redsplatformcommons.service;

public enum ServiceType {
	BUSINESS, PLATFORM;
}
