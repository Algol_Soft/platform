package com.reds.platform.redsplatformcommons.command;

public class LogsPathCmd  extends AbstractCommand{
	
	private String serviceName;
	private String serviceVersion;
	
	public LogsPathCmd() {
		super(CommandType.LOG_PATH);
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceVersion() {
		return serviceVersion;
	}

	public void setServiceVersion(String serviceVersion) {
		this.serviceVersion = serviceVersion;
	}
	

}
