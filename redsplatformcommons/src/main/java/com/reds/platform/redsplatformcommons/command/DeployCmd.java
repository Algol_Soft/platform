package com.reds.platform.redsplatformcommons.command;

/**
 * <b>Purpose:</b> Command argument for deploy service Command
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public class DeployCmd extends AbstractCommand {

	/**
	 * To hold zip location of the service
	 */
	private String location;

	public DeployCmd() {
		super(CommandType.DEPLOY);
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
