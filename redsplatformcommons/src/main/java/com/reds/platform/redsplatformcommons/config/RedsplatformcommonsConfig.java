package com.reds.platform.redsplatformcommons.config;

import com.reds.library.redscommons.config.PlatformConfig;


/**
 * <b>Purpose:</b> This class hold all the configuration related to redsplatformcommons.
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 * This code is generated by Reds on 07/11/2018
 */
public class RedsplatformcommonsConfig implements PlatformConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Holds the name of the configurations
	 */
	private String id="RedsplatformcommonsConfig";	
	/**
	 * Holds the description of the configurations
	 */
	private String description="Configurations for redsplatformcommons";

	/**
	 * To get Unique Id for this RedsplatformcommonsConfig
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * To get description of RedsplatformcommonsConfig
	 */
	@Override
	public String getDescription(){
	 return description;	
	}


}