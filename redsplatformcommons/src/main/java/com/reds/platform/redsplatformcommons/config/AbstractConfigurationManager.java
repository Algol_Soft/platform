package com.reds.platform.redsplatformcommons.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.reds.library.redscommons.config.PlatformConfig;
import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.exception.RedscommonsErrorCodes;
import com.reds.library.redscommons.exception.RedscommonsException;
import com.reds.library.redscommons.xml.RedsXmlManager;
import com.reds.library.redscommons.xml.RedsXmlManagerImpl;

/**
 * <b>Purpose:</b> Abstract base class for all configuration manager
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public abstract class AbstractConfigurationManager {

	/**
	 * 
	 */
	private Map<String, RedsXmlManager<PlatformConfig>> xmlManagers = null;
	/**
	 * 
	 */
	private String location;

	/**
	 * @param location
	 */
	public AbstractConfigurationManager(String location) {
		this.xmlManagers = new HashMap<>();
		this.location = location;
	}

	/**
	 * <b>Purpose:</b> To initialize Reds Configuration. <br>
	 * 1. Check if Config already in disk <br>
	 * 2. if Yes Return the already persisted config <br>
	 * 3. If No Persist the new one and return the new one <br>
	 * 
	 * @param config
	 * @return RedsConfig
	 * @throws RedscommonsException
	 */
	protected PlatformConfig initConfig(PlatformConfig config) throws RedscommonsException {
		try {
			registerConfig(config.getClass());
			RedsXmlManager<PlatformConfig> xmlManager = getManager(config.getClass());
			if (xmlManager.exist(config)) {
				return xmlManager.read(config, config.getClass());
			} else {
				xmlManager.write(config.getDefault());
				return config;
			}
		} catch (RedsXmlException e) {
			throw new RedscommonsException(RedscommonsErrorCodes.errorCode(1),
					"Failed to init configuration " + config.getDescription(), e);
		}
	}

	/**
	 * <b>Purpose:</b>To register xml manager against configuration object
	 * 
	 * @param clazz
	 */
	public void registerConfig(@SuppressWarnings("rawtypes") Class clazz) {
		this.xmlManagers.put(clazz.getName(), new RedsXmlManagerImpl<>(location));
	}

	/**
	 * <b>Purpose:</b>To unRegister xml manager against configuration object
	 * 
	 * @param clazz
	 */
	public void unRegisterConfig(@SuppressWarnings("rawtypes") Class clazz) {
		this.xmlManagers.remove(clazz.getName());
	}

	/**
	 * <b>Purpose:</b>To read a configuration
	 * 
	 * @param config
	 * @return RedsConfig
	 * @throws RedscommonsException
	 */
	public PlatformConfig read(PlatformConfig config) throws RedscommonsException {
		try {
			RedsXmlManager<PlatformConfig> xmlManager = getManager(config.getClass());
			return xmlManager.read(config, config.getClass());
		} catch (RedsXmlException e) {
			throw new RedscommonsException(RedscommonsErrorCodes.errorCode(1),
					"Failed to read configuration " + config.getDescription(), e);
		}
	}

	/**
	 * <b>Purpose:</b>To write a configuration
	 * 
	 * @param config
	 * @throws RedscommonsException
	 */
	public void write(PlatformConfig config) throws RedscommonsException {
		try {
			RedsXmlManager<PlatformConfig> xmlManager = getManager(config.getClass());
			xmlManager.write(config);
		} catch (RedsXmlException e) {
			throw new RedscommonsException(RedscommonsErrorCodes.errorCode(1),
					"Failed to write configuration " + config.getDescription(), e);
		}
	}

	protected void delete(PlatformConfig config) throws RedscommonsException {
		try {
			RedsXmlManager<PlatformConfig> xmlManager = getManager(config.getClass());
			xmlManager.delete(config);
		} catch (RedsXmlException e) {
			throw new RedscommonsException(RedscommonsErrorCodes.errorCode(1),
					"Failed to delete configuration " + config.getDescription(), e);
		}

	}

	public List<PlatformConfig> readAll(Class clazz) throws RedscommonsException {
		try {
			RedsXmlManager<PlatformConfig> xmlManager = getManager(clazz);
			return xmlManager.readAll(clazz);
		} catch (RedsXmlException e) {
			throw new RedscommonsException(RedscommonsErrorCodes.errorCode(1),
					"Failed to readAll configuration " + clazz, e);
		}

	}

	/**
	 * <b>Purpose:</b> To get xml Manager
	 * 
	 * @param config
	 * @return RedsXmlManager
	 * @throws RedscommonsException
	 */
	private RedsXmlManager<PlatformConfig> getManager(Class clazz) throws RedscommonsException {
		RedsXmlManager<PlatformConfig> xmlManager = this.xmlManagers.get(clazz.getName());
		if (xmlManager == null) {
			throw new RedscommonsException(RedscommonsErrorCodes.errorCode(1), "Failed to get XML Manager");
		}
		return xmlManager;
	}

	public boolean isExist(PlatformConfig config) throws RedscommonsException {
		RedsXmlManager<PlatformConfig> xmlManager = getManager(config.getClass());
		try {
			if (xmlManager.exist(config)) {
				return true;
			} else {

				return false;
			}
		} catch (RedsXmlException e) {
			throw new RedscommonsException(RedscommonsErrorCodes.errorCode(1),
					"Failed to check is  configuration exist or not" + config, e);
		}
	}
	
	

}
