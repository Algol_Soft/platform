package com.reds.platform.redsplatformcommons.command;

/**
 * <b>Purpose:</b> Command argument for list service Command
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public class ListCmd extends AbstractCommand {
	/**
	 * -m for market type
	 */
	private String type;

	public ListCmd() {
		super(CommandType.LIST);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
