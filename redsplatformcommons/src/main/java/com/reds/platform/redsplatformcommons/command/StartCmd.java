package com.reds.platform.redsplatformcommons.command;

/**
 * <b>Purpose:</b> Command argument for start service Command
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public class StartCmd extends AbstractCommand {

	private String name;
	private String version;

	public StartCmd() {
		super(CommandType.START_SERVICE);
	}

	public final String getName() {
		return name;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final String getVersion() {
		return version;
	}

	public final void setVersion(String version) {
		this.version = version;
	}

}
