package com.reds.platform.redsplatformcommons.service;

public enum ServiceStatus {
	DEPLOYED,INITIALIZED,STARTED,CLOSED,STOPPED,UNDEPLOYED,NOT_DEPLOYED;

}
