package com.reds.platform.redsplatformcommons.command;

public class SystemInfo {
	private String userName;
	private String platformDeploymentPath;
	private String os;
	private String osVersion;
	private String osArch;
	/**
	 * Returns the number of processors available to the Java virtual machine.
	 *
	 * This value may change during a particular invocation of the virtualmachine.
	 * Applications that are sensitive to the number of availableprocessors should
	 * therefore occasionally poll this property and adjusttheir resource usage
	 * appropriately.
	 *
	 */
	private int availableProcessors;

	private String hardDiskAvailableSpace;
	private String javaVersion;
	/**
	 * Returns the amount of free memory in the Java Virtual Machine.Calling the gc
	 * method may result in increasing the value returnedby freeMemory.
	 */
	private long freeMemory;
	/**
	 * Returns the total amount of memory in the Java virtual machine.The value
	 * returned by this method may vary over time, depending onthe host environment.
	 *
	 * Note that the amount of memory required to hold an object of anygiven type
	 * may be implementation-dependent.
	 *
	 */
	private long totalMemory;
	/**
	 * Returns the maximum amount of memory that the Java virtual machinewill
	 * attempt to use. If there is no inherent limit then the value
	 * java.lang.Long.MAX_VALUE will be returned.
	 */
	private long maxMemory;

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getHardDiskAvailableSpace() {
		return hardDiskAvailableSpace;
	}

	public void setHardDiskAvailableSpace(String hardDiskAvailableSpace) {
		this.hardDiskAvailableSpace = hardDiskAvailableSpace;
	}

	public String getJavaVersion() {
		return javaVersion;
	}

	public void setJavaVersion(String javaVersion) {
		this.javaVersion = javaVersion;
	}

	public long getFreeMemory() {
		return freeMemory;
	}

	public void setFreeMemory(long freeMemory) {
		this.freeMemory = freeMemory;
	}

	public long getTotalMemory() {
		return totalMemory;
	}

	public void setTotalMemory(long totalMemory) {
		this.totalMemory = totalMemory;
	}

	public long getMaxMemory() {
		return maxMemory;
	}

	public void setMaxMemory(long maxMemory) {
		this.maxMemory = maxMemory;
	}

	public int getAvailableProcessors() {
		return availableProcessors;
	}

	public void setAvailableProcessors(int availableProcessors) {
		this.availableProcessors = availableProcessors;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getOsArch() {
		return osArch;
	}

	public void setOsArch(String osArch) {
		this.osArch = osArch;
	}

	public String getPlatformDeploymentPath() {
		return platformDeploymentPath;
	}

	public void setPlatformDeploymentPath(String platformDeploymentPath) {
		this.platformDeploymentPath = platformDeploymentPath;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "SystemInfo [userName=" + userName + ", platformDeploymentPath=" + platformDeploymentPath + ", os=" + os
				+ ", osVersion=" + osVersion + ", osArch=" + osArch + ", availableProcessors=" + availableProcessors
				+ ", hardDiskAvailableSpace=" + hardDiskAvailableSpace + ", javaVersion=" + javaVersion
				+ ", freeMemory=" + freeMemory + ", totalMemory=" + totalMemory + ", maxMemory=" + maxMemory + "]";
	}

}
