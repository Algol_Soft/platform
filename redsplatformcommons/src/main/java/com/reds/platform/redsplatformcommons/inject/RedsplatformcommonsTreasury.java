package com.reds.platform.redsplatformcommons.inject;

/**
 * <b>Purpose:</b> Treasury class provides the way to all the classes of this
 * module to injected objects
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 * This code is generated by Reds on 07/11/2018
 */
public class RedsplatformcommonsTreasury {

	/**
	 * Instance of Component Class
	 */
	public static RedsplatformcommonsComponent open;

	static {
		/* Initialising Component Object */
		RedsplatformcommonsTreasury.open = DaggerRedsplatformcommonsComponent.create();
	}

}
